# Hon(ourable) Pigeon
A reverse proxy, web server and ACME client named after the pigeon Paddy.

This project does not try to be general purpose but is instead tailored to my current personal needs.
For a general purpose tool take a look at [caddy] or build your own tailored one using [certmagic].

[caddy]: https://caddyserver.com/
[certmagic]: https://github.com/caddyserver/certmagic
