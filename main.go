package pigeon

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"hon-pigeon/config"

	"github.com/caddyserver/certmagic"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
)

type Pigeon struct {
	rootContext  context.Context
	httpsServers []*pServer
	httpServer   *http.Server
	magic        *certmagic.Config
	logger       *zap.Logger
	httpLogger   *zap.Logger
	now          func() time.Time
}

func NewPigeon(log *zap.Logger, cfg *config.Config) (*Pigeon, error) {
	p := &Pigeon{
		rootContext: context.Background(),
		logger:      log,
		httpLogger:  log.Named("http"),
		now:         time.Now,
	}
	err := p.initTLS(cfg)
	if err != nil {
		return nil, err
	}
	err = p.initServers(cfg)
	if err != nil {
		return nil, err
	}
	return p, nil
}

func Start() error {
	flag.CommandLine.Init(os.Args[0], flag.ContinueOnError)
	configFile := flag.String("config", "pigeon.yaml", "config file")
	err := flag.CommandLine.Parse(os.Args[1:])
	if err != nil {
		if err == flag.ErrHelp {
			return nil
		}
		return fmt.Errorf("flag: %w", err)
	}

	data, err := os.ReadFile(*configFile)
	if err != nil {
		return fmt.Errorf("read config: %w", err)
	}

	cfg := &config.Config{}
	d := yaml.NewDecoder(bytes.NewReader(data))
	d.KnownFields(true)
	err = d.Decode(&cfg)
	if err != nil {
		return fmt.Errorf("parse yaml: %w", err)
	}

	// TODO decide at runtime
	log, err := zap.NewDevelopment()
	if err != nil {
		return err
	}
	p, err := NewPigeon(log, cfg)
	if err != nil {
		return err
	}

	err = p.ListenAndServe()
	if err != nil {
		return err
	}
	return nil
}
