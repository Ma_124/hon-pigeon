package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"hon-pigeon/config"
)

func RedirectHandler(cfg *config.Config, r *config.Redirect, errorHandler ErrorHandler) (http.Handler, error) {
	if r.Port == 0 && r.Path == "" {
		return nil, fmt.Errorf("either port or path must be set")
	}

	return http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		code := http.StatusFound
		if r.Permanent {
			code = http.StatusMovedPermanently
		}
		newURL := "https://"
		if r.Port != 0 {
			newURL += hostOnly(req.Host)
			if r.Port != 443 {
				newURL += ":" + strconv.Itoa(int(r.Port))
			}
		} else {
			newURL += req.Host
		}
		if r.Path != "" {
			if r.Path[0] != '/' {
				newURL += "/"
			}
			newURL += r.Path
		} else {
			if req.RequestURI == "" || req.RequestURI[0] != '/' {
				newURL += "/"
			}
			newURL += req.RequestURI
		}
		http.Redirect(resp, req, newURL, code)
	}), nil
}
