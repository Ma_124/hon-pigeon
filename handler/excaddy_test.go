package handler

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_hostOnly(t *testing.T) {
	tests := []struct {
		name     string
		hostport string
		want     string
	}{
		{"empty", "", ""},
		{"only port", ":80", ""},
		{"localhost, no port", "localhost", "localhost"},
		{"localhost, with port", "localhost:80", "localhost"},
		{"domain, no port", "test.example.org", "test.example.org"},
		{"domain, with port", "test.example.org:80", "test.example.org"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, hostOnly(tt.hostport))
		})
	}
}

func Test_httpRedirectHandler(t *testing.T) {
	tests := []struct {
		name   string
		from   string
		wantTo string
	}{
		{"only scheme", "http://", "https:///"},
		{"only domain", "http://domain", "https://domain/"},
		{"domain with empty path", "http://domain/", "https://domain/"},
		{"domain with port", "http://domain:80", "https://domain/"},
		{"domain with port and query", "http://domain:80?a", "https://domain/?a"},
		{"domain with port, path, and query", "http://domain:80/hello/world?a", "https://domain/hello/world?a"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			resp := httptest.NewRecorder()
			req, err := http.NewRequest("GET", tt.from, nil)
			if err != nil {
				panic(err)
			}

			HttpRedirectHandler(resp, req)
			gotResp := resp.Result()
			assert.Equal(t, []string{"close"}, gotResp.Header["Connection"])
			assert.Equal(t, http.StatusMovedPermanently, gotResp.StatusCode)
			assert.Equal(t, []string{tt.wantTo}, gotResp.Header["Location"])
		})
	}
}
