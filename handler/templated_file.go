package handler

import (
	"bytes"
	"html/template"
	"io"
	"net/http"
	"os"
	"strconv"

	"hon-pigeon/config"
)

func TemplatedFileHandler(cfg *config.Config, file string, errorHandler ErrorHandler) (http.Handler, error) {
	err := checkExistsAndReadable(file)
	if err != nil {
		return nil, err
	}

	return http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		if !enforceMethods(resp, req, getAndHead, errorHandler) {
			return
		}

		data, err := os.ReadFile(file)
		if err != nil {
			AddError(req, err)
			errorHandler(resp, req, http.StatusInternalServerError, "")
			return
		}
		t, err := template.New("template").Parse(string(data))
		if err != nil {
			AddError(req, err)
			errorHandler(resp, req, http.StatusInternalServerError, "")
			return
		}
		var page bytes.Buffer
		err = t.Execute(&page, struct{}{})
		if err != nil {
			AddError(req, err)
			errorHandler(resp, req, http.StatusInternalServerError, "")
			return
		}
		resp.Header().Set("Content-Type", "text/html; charset=utf-8")
		resp.Header().Set("Content-Length", strconv.Itoa(page.Len()))
		resp.WriteHeader(http.StatusOK)

		if req.Method != http.MethodHead {
			_, _ = io.Copy(resp, &page)
		}
	}), err
}
