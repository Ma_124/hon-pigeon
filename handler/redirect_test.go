package handler_test

import (
	"net/http"
	"testing"

	"hon-pigeon/config"
	"hon-pigeon/handler"
	"hon-pigeon/internal/testutil"

	"github.com/gavv/httpexpect/v2"
)

func TestRedirectHandler(t *testing.T) {
	testutil.TestHandler(t, handler.RedirectHandler, []testutil.HandlerTest{
		{
			Name:    "neither path nor port redirect",
			Arg:     &config.Redirect{},
			WantErr: "either port or path must be set",
		},
		{
			Name: "port redirect",
			Arg: &config.Redirect{
				Port: 8080,
			},
			Subtests: []testutil.Subtest{
				{
					Name: "basic request",
					Fn: func(t *testing.T, e *httpexpect.Expect) {
						e.GET("/").
							WithRedirectPolicy(httpexpect.DontFollowRedirects).
							Expect().
							Status(http.StatusFound).
							Header("Location").
							Equal("https://127.0.0.1:8080/")
					},
				},
				{
					Name: "request with host",
					Fn: func(t *testing.T, e *httpexpect.Expect) {
						e.GET("/").
							WithRedirectPolicy(httpexpect.DontFollowRedirects).
							WithHost("example.org").
							Expect().
							Status(http.StatusFound).
							Header("Location").
							Equal("https://example.org:8080/")
					},
				},
				{
					Name: "request with port in host",
					Fn: func(t *testing.T, e *httpexpect.Expect) {
						e.GET("/").
							WithRedirectPolicy(httpexpect.DontFollowRedirects).
							WithHost("example.org:9090").
							Expect().
							Status(http.StatusFound).
							Header("Location").
							Equal("https://example.org:8080/")
					},
				},
				{
					Name: "complex GET",
					Fn: func(t *testing.T, e *httpexpect.Expect) {
						e.GET("/hällo/world").
							WithQueryString("somequery&foo=bar").
							WithRedirectPolicy(httpexpect.DontFollowRedirects).
							WithHost("example.org:9090").
							Expect().
							Status(http.StatusFound).
							Header("Location").
							Equal("https://example.org:8080/h%C3%A4llo/world?foo=bar&somequery=")
					},
				},
			},
		},
		{
			Name: "permanent port redirect",
			Arg: &config.Redirect{
				Port:      8080,
				Permanent: true,
			},
			Subtests: []testutil.Subtest{
				{
					Name: "complex GET",
					Fn: func(t *testing.T, e *httpexpect.Expect) {
						e.GET("/hällo/world").
							WithQueryString("somequery&foo=bar").
							WithRedirectPolicy(httpexpect.DontFollowRedirects).
							WithHost("example.org:9090").
							Expect().
							Status(http.StatusMovedPermanently).
							Header("Location").
							Equal("https://example.org:8080/h%C3%A4llo/world?foo=bar&somequery=")
					},
				},
			},
		},
		{
			Name: "path redirect",
			Arg: &config.Redirect{
				Path: "/hällo",
			},
			Subtests: []testutil.Subtest{
				{
					Name: "complex GET",
					Fn: func(t *testing.T, e *httpexpect.Expect) {
						e.GET("/test/hällo/world").
							WithQueryString("somequery&foo=bar").
							WithRedirectPolicy(httpexpect.DontFollowRedirects).
							WithHost("example.org:9090").
							Expect().
							Status(http.StatusFound).
							Header("Location").
							Equal("https://example.org:9090/h%c3%a4llo")
					},
				},
			},
		},
		{
			Name: "permanent path redirect",
			Arg: &config.Redirect{
				Path:      "/hällo",
				Permanent: true,
			},
			Subtests: []testutil.Subtest{
				{
					Name: "complex GET",
					Fn: func(t *testing.T, e *httpexpect.Expect) {
						e.GET("/test/hällo/world").
							WithQueryString("somequery&foo=bar").
							WithRedirectPolicy(httpexpect.DontFollowRedirects).
							WithHost("example.org:9090").
							Expect().
							Status(http.StatusMovedPermanently).
							Header("Location").
							Equal("https://example.org:9090/h%c3%a4llo")
					},
				},
			},
		},
		{
			Name: "permanent path and port redirect",
			Arg: &config.Redirect{
				Path:      "/hällo",
				Port:      8080,
				Permanent: true,
			},
			Subtests: []testutil.Subtest{
				{
					Name: "complex GET",
					Fn: func(t *testing.T, e *httpexpect.Expect) {
						e.GET("/test/hällo/world").
							WithQueryString("somequery&foo=bar").
							WithRedirectPolicy(httpexpect.DontFollowRedirects).
							WithHost("example.org:9090").
							Expect().
							Status(http.StatusMovedPermanently).
							Header("Location").
							Equal("https://example.org:8080/h%c3%a4llo")
					},
				},
			},
		},
		{
			Name: "path and port redirect to 443",
			Arg: &config.Redirect{
				Path: "/hällo",
				Port: 443,
			},
			Subtests: []testutil.Subtest{
				{
					Name: "complex GET",
					Fn: func(t *testing.T, e *httpexpect.Expect) {
						e.GET("/test/hällo/world").
							WithQueryString("somequery&foo=bar").
							WithRedirectPolicy(httpexpect.DontFollowRedirects).
							WithHost("example.org:9090").
							Expect().
							Status(http.StatusFound).
							Header("Location").
							Equal("https://example.org/h%c3%a4llo")
					},
				},
			},
		},
	})
}
