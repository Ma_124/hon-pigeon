package handler

import (
	"errors"
	"net/http"

	"hon-pigeon/config"
)

func ReverseProxy(cfg *config.Config, upstream string, errorHandler ErrorHandler) (http.Handler, error) {
	return http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		// TODO
		AddError(req, errors.New("not implemented"))
		errorHandler(resp, req, http.StatusInternalServerError, "")
	}), nil
}
