package handler

import (
	"bytes"
	"io"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func TestErrorsInContext(t *testing.T) {
	t.Run("no errors with background context", func(t *testing.T) {
		req, err := http.NewRequest("GET", "https://localhost", nil)
		require.NoError(t, err)
		req = WithErrorsRecorder(req)
		assert.Nil(t, GetErrors(req.Context()))
	})
	t.Run("no errors", func(t *testing.T) {
		req, err := http.NewRequest("GET", "https://localhost", nil)
		require.NoError(t, err)
		req = WithErrorsRecorder(req)
		assert.Nil(t, GetErrors(req.Context()))
	})
	t.Run("an error", func(t *testing.T) {
		req, err := http.NewRequest("GET", "https://localhost", nil)
		require.NoError(t, err)
		req = WithErrorsRecorder(req)
		AddError(req, io.EOF)
		assert.Equal(t, []error{io.EOF}, GetErrors(req.Context()))
	})
	t.Run("two errors", func(t *testing.T) {
		req, err := http.NewRequest("GET", "https://localhost", nil)
		require.NoError(t, err)
		req = WithErrorsRecorder(req)
		AddError(req, io.EOF)
		AddError(req, io.ErrUnexpectedEOF)
		assert.Equal(t, []error{io.EOF, io.ErrUnexpectedEOF}, GetErrors(req.Context()))
	})
	t.Run("add error with no error list", func(t *testing.T) {
		assert.PanicsWithValue(t, "AddError: no error list attached to this context", func() {
			req, err := http.NewRequest("GET", "https://localhost", nil)
			require.NoError(t, err)
			AddError(req, io.EOF)
		})
	})
	t.Run("get errors with no error list", func(t *testing.T) {
		assert.PanicsWithValue(t, "GetErrors: no error list attached to this context", func() {
			req, err := http.NewRequest("GET", "https://localhost", nil)
			require.NoError(t, err)
			GetErrors(req.Context())
		})
	})
}

func timeSequence(times ...string) func() time.Time {
	i := 0

	return func() time.Time {
		t, err := time.Parse(time.RFC3339, times[i])
		i++
		if err != nil {
			panic(err)
		}
		return t
	}
}

type nopSyncer struct {
	io.Writer
}

func (nopSyncer) Sync() error {
	return nil
}

func Test_withLogger(t *testing.T) {
	var buf bytes.Buffer
	c := zap.NewProductionEncoderConfig()
	c.TimeKey = zapcore.OmitKey
	l := zap.New(zapcore.NewCore(zapcore.NewJSONEncoder(c), nopSyncer{&buf}, zapcore.DebugLevel))
	h := WithLogger(l, timeSequence(
		"2000-01-01T00:00:00Z", "2000-01-01T00:00:02Z",
		"2000-01-01T00:00:00Z", "2000-01-01T00:00:02Z",
		"2000-01-01T00:10:13Z", "2000-01-01T00:13:13Z",
		"2000-01-01T00:00:00Z", "2000-01-01T00:00:02Z",
	), http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		if req.URL.Query().Get("err") == "y" {
			AddError(req, io.EOF)
		}

		if req.URL.Path == "/panic" || req.URL.Path == "panic" {
			panic("test")
		}

		resp.Header().Set("Server", "hon-pigeon-test")
		resp.WriteHeader(http.StatusOK)
		_, err := resp.Write([]byte("Hello World"))
		if err != nil {
			panic(err)
		}
	}))
	resp := httptest.NewRecorder()

	t.Run("normal get", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/hello", nil)
		if err != nil {
			panic(err)
		}
		h.ServeHTTP(resp, req)
		assert.Equal(t, `
{"level":"info","msg":"served","time":946684802,"method":"GET","uri":"/hello","status":200,"remote":"","body-size":11,"latency":2}
`[1:], buf.String())
		buf.Truncate(0)
	})

	t.Run("panic", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/panic", nil)
		if err != nil {
			panic(err)
		}
		h.ServeHTTP(resp, req)
		r := regexp.MustCompile(`
\{"level":"info","msg":"served","time":946684802,"method":"GET","uri":"/panic","status":0,"panic":\{"stacktrace":"goroutine [0-9]+.*hon-pigeon/handler\.WithLogger\.func1\.1\(\).*","value":"test"},"remote":"","body-size":0,"latency":2}
`)
		assert.Regexp(t, r, "\n"+buf.String())
		buf.Truncate(0)
	})

	t.Run("normal post with body", func(t *testing.T) {
		req, err := http.NewRequest("POST", "https://example.com/world?a", io.LimitReader(rand.New(rand.NewSource(0)), 123))
		if err != nil {
			panic(err)
		}
		h.ServeHTTP(resp, req)
		assert.Equal(t, `
{"level":"info","msg":"served","time":946685593,"method":"POST","uri":"https://example.com/world?a","status":200,"remote":"","body-size":11,"latency":180}
`[1:], buf.String())
		buf.Truncate(0)
	})

	t.Run("error", func(t *testing.T) {
		req, err := http.NewRequest("POST", "https://example.com/world?err=y", io.LimitReader(rand.New(rand.NewSource(0)), 123))
		if err != nil {
			panic(err)
		}
		h.ServeHTTP(resp, req)
		assert.Equal(t, `
{"level":"info","msg":"served","time":946684802,"method":"POST","uri":"https://example.com/world?err=y","status":200,"errors":[{"error":"EOF"}],"remote":"","body-size":11,"latency":2}
`[1:], buf.String())
		buf.Truncate(0)
	})
}
