package handler

import (
	"fmt"
	"net/http"

	"hon-pigeon/config"
)

func NewLeafHandler(
	cfg *config.Config, handlerCfg *config.RouteHandler, name string,
	errorHandler ErrorHandler,
) (http.Handler, error) {
	var previous string
	var h http.Handler
	checkMutex := func(current string) error {
		if previous != "" {
			return fmt.Errorf("%s: %s and %s are mutually exclusive", name, current, previous)
		}
		previous = current
		return nil
	}

	if handlerCfg.File != "" {
		err := checkMutex("file")
		if err != nil {
			return nil, fmt.Errorf("%s: %w", name, err)
		}
		h, err = FileHandler(cfg, handlerCfg.File, errorHandler)
		if err != nil {
			return nil, fmt.Errorf("%s: file: %w", name, err)
		}
	}
	if handlerCfg.TemplatedFile != "" {
		err := checkMutex("templated-file")
		if err != nil {
			return nil, fmt.Errorf("%s: %w", name, err)
		}
		h, err = FileHandler(cfg, handlerCfg.TemplatedFile, errorHandler)
		if err != nil {
			return nil, fmt.Errorf("%s: templated-file: %w", name, err)
		}
	}
	if handlerCfg.Directory != "" {
		err := checkMutex("directory")
		if err != nil {
			return nil, fmt.Errorf("%s: %w", name, err)
		}
		h, err = DirectoryHandler(cfg, handlerCfg.Directory, nil, errorHandler)
		if err != nil {
			return nil, fmt.Errorf("%s: directory: %w", name, err)
		}
	}
	if handlerCfg.Redirect != nil {
		err := checkMutex("redirect")
		if err != nil {
			return nil, err
		}
		h, err = RedirectHandler(cfg, handlerCfg.Redirect, errorHandler)
		if err != nil {
			return nil, fmt.Errorf("%s: redirect: %w", name, err)
		}
	}
	if handlerCfg.ReverseProxy != "" {
		err := checkMutex("reverse-proxy")
		if err != nil {
			return nil, err
		}
		h, err = ReverseProxy(cfg, handlerCfg.ReverseProxy, errorHandler)
		if err != nil {
			return nil, fmt.Errorf("%s: reverse-proxy: %w", name, err)
		}
	}

	if h == nil {
		return nil, fmt.Errorf("%s: no handler specified", name)
	}

	return h, nil
}
