package handler_test

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"testing"

	"hon-pigeon/handler"
	"hon-pigeon/internal/testutil"

	"github.com/gavv/httpexpect/v2"
	"github.com/stretchr/testify/require"
)

func TestTemplatedFileHandler(t *testing.T) {
	f, err := os.CreateTemp("", "hon-pigeon-test")
	if err != nil {
		panic(err)
	}
	defer func() {
		_ = f.Close()
	}()
	testutil.WithTempFS(testutil.ExampleDir, func(base string) {
		testutil.TestHandler(t, handler.TemplatedFileHandler, []testutil.HandlerTest{
			{
				Name:    "invalid file",
				Arg:     filepath.Join(base, "a"),
				WantErr: fmt.Sprintf("open %s: no such file or directory", filepath.Join(base, "a")),
			},
			{
				Name: "html file",
				Arg:  filepath.Join(base, "example-dir/index.html"),
				Subtests: []testutil.Subtest{
					{
						Name: "GET",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusOK).
								ContentType("text/html", "utf-8").
								Body().
								Equal("Some <b>HTML</b>")
						},
					},
					{
						Name: "HEAD",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							r := e.HEAD("/").Expect().
								Status(http.StatusOK).
								ContentType("text/html", "utf-8")
							r.Header("Content-Length").Equal("16")
							r.Body().Empty()
						},
					},
					{
						Name: "OPTIONS",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.OPTIONS("/").Expect().
								Status(http.StatusNoContent).
								NoContent().
								Header("Allow").Equal("OPTIONS, GET, HEAD")
						},
					},
					{
						Name: "unallowed method",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.POST("/").Expect().
								Status(http.StatusMethodNotAllowed).
								Body().
								Equal("only OPTIONS, GET, HEAD are allowed\n")
						},
					},
					{
						Name: "invalid method",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.Request("INV", "/").Expect().
								Status(http.StatusMethodNotAllowed).
								Body().
								Equal("only OPTIONS, GET, HEAD are allowed\n")
						},
					},
				},
			},
			{
				Name: "js file with uncommon name",
				Arg:  filepath.Join(base, "example-dir/sub/täst.js"),
				Subtests: []testutil.Subtest{
					{
						Name: "GET",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusOK).
								ContentType("text/html", "utf-8").
								Body().
								Equal(`alert("Täst")`)
						},
					},
				},
			},
			{
				Name: "actual template",
				Arg:  filepath.Join(base, "example-dir/noindex/template.html"),
				Subtests: []testutil.Subtest{
					{
						Name: "GET",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusOK).
								ContentType("text/html", "utf-8").
								Body().
								Equal(`Some <a href="%22">&lt;Template&gt;</a>`)
						},
					},
					{
						Name: "HEAD",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							r := e.HEAD("/").Expect().
								Status(http.StatusOK).
								ContentType("text/html", "utf-8")
							r.Header("Content-Length").Equal("39")
							r.Body().Empty()
						},
					},
					{
						Name: "OPTIONS",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.OPTIONS("/").Expect().
								Status(http.StatusNoContent).
								NoContent().
								Header("Allow").Equal("OPTIONS, GET, HEAD")
						},
					},
					{
						Name: "unallowed method",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.POST("/").Expect().
								Status(http.StatusMethodNotAllowed).
								Body().
								Equal("only OPTIONS, GET, HEAD are allowed\n")
						},
					},
					{
						Name: "invalid method",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.Request("INV", "/").Expect().
								Status(http.StatusMethodNotAllowed).
								Body().
								Equal("only OPTIONS, GET, HEAD are allowed\n")
						},
					},
				},
			},
			{
				Name: "deleted file",
				Arg:  f.Name(),
				Subtests: []testutil.Subtest{
					{
						Name: "before delete",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusOK).
								Body().Empty()
						},
					},
					{
						Name: "after delete",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							err = os.Remove(f.Name())
							require.NoError(t, err)
							e.GET("/").Expect().
								Status(http.StatusInternalServerError).
								Body().
								Equal("open " + f.Name() + ": no such file or directory\n")
						},
					},
				},
			},
			{
				Name: "broken template",
				Arg:  filepath.Join(base, "example-dir/sub/broken-template.html"),
				Subtests: []testutil.Subtest{
					{
						Name: "GET",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusInternalServerError).
								Body().
								Equal("template: template:1:3: executing \"template\" at <.BrokenDoesNotExist>: can't evaluate field BrokenDoesNotExist in type struct {}\n")
						},
					},
					{
						Name: "HEAD",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.HEAD("/").Expect().
								Status(http.StatusInternalServerError).
								Body().
								Empty()
						},
					},
				},
			},
			{
				Name: "invalid template",
				Arg:  filepath.Join(base, "example-dir/sub/invalid-template.html"),
				Subtests: []testutil.Subtest{
					{
						Name: "GET",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusInternalServerError).
								Body().
								Equal("template: template:1: unclosed action\n")
						},
					},
					{
						Name: "HEAD",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.HEAD("/").Expect().
								Status(http.StatusInternalServerError).
								Body().
								Empty()
						},
					},
				},
			},
			{
				Name: "empty file",
				Arg:  filepath.Join(base, "example-dir/noindex/empty file"),
				Subtests: []testutil.Subtest{
					{
						Name: "GET",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusOK).
								ContentType("text/html", "utf-8").
								Body().
								Empty()
						},
					},
				},
			},
		})
	})
}
