package handler

import (
	"io"
	"net/http"
	"os"

	"hon-pigeon/config"
)

func FileHandler(cfg *config.Config, file string, errorHandler ErrorHandler) (http.Handler, error) {
	err := checkExistsAndReadable(file)
	if err != nil {
		return nil, err
	}

	return http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		if !enforceMethods(resp, req, getAndHead, errorHandler) {
			return
		}

		f, err := os.Open(file)
		if err != nil {
			AddError(req, err)
			errorHandler(resp, req, http.StatusInternalServerError, "")
			return
		}
		defer func() {
			err := f.Close()
			if err != nil {
				AddError(req, err)
			}
		}()

		fi, err := f.Stat()
		if err != nil {
			AddError(req, err)
			errorHandler(resp, req, http.StatusInternalServerError, "")
			return
		}

		http.ServeContent(resp, req, file, fi.ModTime(), f)
	}), nil
}

func checkExistsAndReadable(path string) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer func() {
		err2 := f.Close()
		if err == nil {
			err = err2
		}
	}()
	var data [16]byte
	_, err = io.ReadFull(f, data[:])
	if err != nil && err != io.ErrUnexpectedEOF && err != io.EOF {
		return err
	}
	return nil
}
