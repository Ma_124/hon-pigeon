package handler_test

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"testing"

	"hon-pigeon/handler"
	"hon-pigeon/internal/testutil"

	"github.com/gavv/httpexpect/v2"
	"github.com/stretchr/testify/require"
)

func Test_fileHandler(t *testing.T) {
	f, err := os.CreateTemp("", "hon-pigeon-test")
	if err != nil {
		panic(err)
	}
	defer func() {
		_ = f.Close()
	}()
	testutil.WithTempFS(testutil.ExampleDir, func(base string) {
		testutil.TestHandler(t, handler.FileHandler, []testutil.HandlerTest{
			{
				Name:    "invalid file",
				Arg:     filepath.Join(base, "a"),
				WantErr: fmt.Sprintf("open %s: no such file or directory", filepath.Join(base, "a")),
			},
			{
				Name: "html file",
				Arg:  filepath.Join(base, "example-dir/index.html"),
				Subtests: []testutil.Subtest{
					{
						Name: "GET",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusOK).
								ContentType("text/html", "utf-8").
								Body().
								Equal("Some <b>HTML</b>")
						},
					},
					{
						Name: "HEAD",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							r := e.HEAD("/").Expect().
								Status(http.StatusOK).
								ContentType("text/html", "utf-8")
							r.Header("Content-Length").Equal("16")
							r.Body().Empty()
						},
					},
					{
						Name: "OPTIONS",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.OPTIONS("/").Expect().
								Status(http.StatusNoContent).
								NoContent().
								Header("Allow").Equal("OPTIONS, GET, HEAD")
						},
					},
					{
						Name: "unallowed method",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.POST("/").Expect().
								Status(http.StatusMethodNotAllowed).
								Body().
								Equal("only OPTIONS, GET, HEAD are allowed\n")
						},
					},
					{
						Name: "invalid method",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.Request("INV", "/").Expect().
								Status(http.StatusMethodNotAllowed).
								Body().
								Equal("only OPTIONS, GET, HEAD are allowed\n")
						},
					},
				},
			},
			{
				Name: "js file with uncommon name",
				Arg:  filepath.Join(base, "example-dir/sub/täst.js"),
				Subtests: []testutil.Subtest{
					{
						Name: "GET",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusOK).
								ContentType("text/javascript", "utf-8").
								Body().
								Equal(`alert("Täst")`)
						},
					},
					{
						Name: "HEAD",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							r := e.HEAD("/").Expect().
								Status(http.StatusOK).
								ContentType("text/javascript", "utf-8")
							r.Header("Content-Length").Equal("14")
							r.Body().Empty()
						},
					},
					{
						Name: "OPTIONS",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.OPTIONS("/").Expect().
								Status(http.StatusNoContent).
								NoContent().
								Header("Allow").Equal("OPTIONS, GET, HEAD")
						},
					},
					{
						Name: "unallowed method",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.POST("/").Expect().
								Status(http.StatusMethodNotAllowed).
								Body().
								Equal("only OPTIONS, GET, HEAD are allowed\n")
						},
					},
					{
						Name: "invalid method",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.Request("INV", "/").Expect().
								Status(http.StatusMethodNotAllowed).
								Body().
								Equal("only OPTIONS, GET, HEAD are allowed\n")
						},
					},
				},
			},
			{
				Name: "deleted file",
				Arg:  f.Name(),
				Subtests: []testutil.Subtest{
					{
						Name: "before delete",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.Request("GET", "/").Expect().Status(http.StatusOK)
						},
					},
					{
						Name: "after delete",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							err = os.Remove(f.Name())
							require.NoError(t, err)
							e.Request("GET", "/").Expect().Status(http.StatusInternalServerError)
						},
					},
				},
			},
			{
				Name: "empty file",
				Arg:  filepath.Join(base, "example-dir/noindex/empty file"),
				Subtests: []testutil.Subtest{
					{
						Name: "GET",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusOK).
								ContentType("text/plain", "utf-8").
								Body().
								Empty()
						},
					},
				},
			},
			{
				Name: "short and binary file",
				Arg:  filepath.Join(base, "example-dir/noindex/binary"),
				Subtests: []testutil.Subtest{
					{
						Name: "GET",
						Fn: func(t *testing.T, e *httpexpect.Expect) {
							e.GET("/").Expect().
								Status(http.StatusOK).
								ContentType("application/octet-stream").
								Body().
								Equal("\x01")
						},
					},
				},
			},
		})
	})
}
