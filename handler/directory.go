package handler

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"path/filepath"

	"hon-pigeon/config"
)

func DirectoryHandler(cfg *config.Config, rawDir string, index *template.Template, errorHandler ErrorHandler) (http.Handler, error) {
	dir, err := filepath.Abs(rawDir)
	if err != nil {
		return nil, fmt.Errorf("absolute path: %w", err)
	}
	err = checkExistsAndListable(dir)
	if err != nil {
		return nil, err
	}

	return http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		// TODO
		AddError(req, errors.New("not implemented"))
		errorHandler(resp, req, http.StatusInternalServerError, "")
	}), nil
}

func checkExistsAndListable(path string) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer func() {
		err2 := f.Close()
		if err == nil {
			err = err2
		}
	}()
	_, err = f.ReadDir(1)
	if err != nil && err != io.EOF {
		return err
	}
	return nil
}
