package handler

import (
	"fmt"
	"net/http"

	"hon-pigeon/config"
	pigeon "hon-pigeon/matcher"
)

type ErrorHandler func(resp http.ResponseWriter, req *http.Request, status int, msg string)

var allMethods = []string{
	http.MethodGet,
	http.MethodHead,
	http.MethodPost,
	http.MethodPut,
	http.MethodPatch,
	http.MethodDelete,
	http.MethodConnect,
	http.MethodOptions,
	http.MethodTrace,
}

var getAndHead = map[string]struct{}{
	http.MethodGet:  {},
	http.MethodHead: {},
}

func NewHandler(cfg *config.Config, srvCfg *config.Server) (http.Handler, error) {
	routesByHost := map[string][]*config.Route{}
	for _, routeGroup := range srvCfg.Routes {
		g := cfg.Routes[routeGroup]
		if g == nil {
			return nil, fmt.Errorf("unknown route group %q", routeGroup)
		}
		for _, route := range g {
			for _, host := range route.Matcher.Host {
				routesByHost[host] = append(routesByHost[host], route)
			}
		}
	}

	dm := pigeon.NewDomainMatcher[http.HandlerFunc]()

	for host, routes := range routesByHost {
		err := dm.Add(host, func(resp http.ResponseWriter, req *http.Request) {
			// TODO
			_ = routes
		})
		if err != nil {
			return nil, err
		}
	}

	// TODO implement
	return http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		_, _ = resp.Write([]byte("Ok"))
	}), nil
}

func listMethods(methods map[string]struct{}) string {
	s := "OPTIONS"
	for _, m := range allMethods {
		if _, ok := methods[m]; ok {
			s += ", " + m
		}
	}
	return s
}

func enforceMethods(resp http.ResponseWriter, req *http.Request, methods map[string]struct{}, errorHandler ErrorHandler) bool {
	if req.Method == http.MethodOptions {
		resp.Header().Set("Allow", listMethods(methods))
		resp.WriteHeader(http.StatusNoContent)
		return false
	}
	if _, ok := methods[req.Method]; !ok {
		errorHandler(resp, req, http.StatusMethodNotAllowed, "only "+listMethods(methods)+" are allowed")
		return false
	}
	return true
}
