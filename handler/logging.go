package handler

import (
	"context"
	"io"
	"net/http"
	"runtime"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type errorsKey struct{}
type errorsValue struct {
	errors []error
}

func WithErrorsRecorder(req *http.Request) *http.Request {
	return req.WithContext(context.WithValue(req.Context(), errorsKey{}, &errorsValue{}))
}

func AddError(req *http.Request, err error) {
	ctx := req.Context()
	v, ok := ctx.Value(errorsKey{}).(*errorsValue)
	if v == nil || !ok {
		panic("AddError: no error list attached to this context")
	}
	v.errors = append(v.errors, err)
}

func GetErrors(ctx context.Context) []error {
	v, ok := ctx.Value(errorsKey{}).(*errorsValue)
	if v == nil || !ok {
		panic("GetErrors: no error list attached to this context")
	}
	return v.errors
}

func WithLogger(httpLogger *zap.Logger, now func() time.Time, next http.Handler) http.Handler {
	return http.HandlerFunc(func(rawResp http.ResponseWriter, req *http.Request) {
		startTime := now()
		originalURI := req.URL.String()
		resp := loggerRespWriter{Up: rawResp}
		req = WithErrorsRecorder(req)

		defer func() {
			panicError := recover()
			stopTime := now()

			errorsField := zap.Skip()
			if errors := GetErrors(req.Context()); len(errors) > 0 {
				errorsField = zap.Errors("errors", errors)
			}

			panicField := zap.Skip()
			if panicError != nil {
				panicField = zap.Object("panic", panicInfo(panicError))
			}

			httpLogger.Info("served",
				zap.Time("time", stopTime),
				zap.String("method", req.Method),
				zap.String("uri", originalURI),
				zap.Int("status", resp.Status),
				errorsField,
				panicField,
				zap.String("remote", req.RemoteAddr),
				zap.Int("body-size", resp.BodySize),
				zap.Duration("latency", stopTime.Sub(startTime)),
			)
		}()

		next.ServeHTTP(&resp, req)
	})
}

func panicInfo(panicError any) zapcore.ObjectMarshaler {
	stackTrace := make([]byte, 1024)
	n := runtime.Stack(stackTrace, false)
	return zapcore.ObjectMarshalerFunc(func(o zapcore.ObjectEncoder) error {
		o.AddByteString("stacktrace", stackTrace[:n])
		return o.AddReflected("value", panicError)
	})
}

type loggerRespWriter struct {
	Up       http.ResponseWriter
	Status   int
	BodySize int
}

func (w *loggerRespWriter) Header() http.Header {
	return w.Up.Header()
}

func (w *loggerRespWriter) WriteHeader(statusCode int) {
	w.Status = statusCode
	w.Up.WriteHeader(statusCode)
}

func (w *loggerRespWriter) Write(bytes []byte) (int, error) {
	n, err := w.Up.Write(bytes)
	w.BodySize += n
	return n, err
}

func (w *loggerRespWriter) WriteString(s string) (int, error) {
	n, err := io.WriteString(w.Up, s)
	w.BodySize += n
	return n, err
}

func (w *loggerRespWriter) Flush() {
	if f, ok := w.Up.(http.Flusher); ok {
		f.Flush()
	}
}

func (w *loggerRespWriter) Push(target string, opts *http.PushOptions) error {
	if p, ok := w.Up.(http.Pusher); ok {
		return p.Push(target, opts)
	}
	return http.ErrNotSupported
}
