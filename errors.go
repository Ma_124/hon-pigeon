package pigeon

type ErrorCollection []error

func (e *ErrorCollection) Add(err error) *ErrorCollection {
	if err == nil {
		return e
	}
	*e = append(*e, err)
	return e
}

func (e *ErrorCollection) IntoError() error {
	switch len(*e) {
	case 0:
		return nil
	case 1:
		return (*e)[0]
	default:
		return errors(*e)
	}
}

type errors []error

func (e errors) Error() string {
	msg := "multiple errors:\n"
	for _, err := range e {
		msg += err.Error() + "\n"
	}
	return msg
}
