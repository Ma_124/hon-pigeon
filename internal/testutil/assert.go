package testutil

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func AssertEqError(t *testing.T, theError error, errString string, msgAndArgs ...interface{}) bool {
	t.Helper()
	if errString == "" {
		return assert.NoError(t, theError, msgAndArgs...)
	} else {
		return assert.EqualError(t, theError, errString, msgAndArgs...)
	}
}
