package testutil

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"hon-pigeon/config"
	"hon-pigeon/handler"

	"github.com/gavv/httpexpect/v2"
)

func errorHandler(resp http.ResponseWriter, req *http.Request, status int, msg string) {
	resp.WriteHeader(status)
	if req.Method == http.MethodHead {
		return
	}
	if len(msg) > 0 {
		_, err := fmt.Fprintf(resp, "%s\n", msg)
		if err != nil {
			panic(err)
		}
	}
	for _, err := range handler.GetErrors(req.Context()) {
		_, err = fmt.Fprintf(resp, "%s\n", err)
		if err != nil {
			panic(err)
		}
	}
}

type HandlerTest struct {
	Name     string
	Arg      interface{}
	WantErr  string
	Subtests []Subtest
}

type Subtest = struct {
	Name string
	Fn   func(t *testing.T, e *httpexpect.Expect)
}

func TestHandler[V any](t *testing.T, f func(*config.Config, V, handler.ErrorHandler) (http.Handler, error), tests []HandlerTest) {
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			h, err := f(nil, tt.Arg.(V), errorHandler)
			if !AssertEqError(t, err, tt.WantErr) {
				return
			}
			if len(tt.Subtests) == 0 {
				return
			}
			s := httptest.NewServer(http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
				h.ServeHTTP(resp, handler.WithErrorsRecorder(req))
			}))
			defer s.Close()

			for _, stt := range tt.Subtests {
				t.Run(stt.Name, func(t *testing.T) {
					e := httpexpect.New(t, s.URL)
					stt.Fn(t, e)
				})
			}
		})
	}
}
