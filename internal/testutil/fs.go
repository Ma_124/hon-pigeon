package testutil

import (
	"embed"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
)

//go:embed example-dir
var ExampleDir embed.FS

func WithTempFS(filesystem fs.FS, f func(path string)) {
	tmp, err := os.MkdirTemp("", "hon-pigeon-test")
	if err != nil {
		panic(err)
	}
	defer func() {
		err := os.RemoveAll(tmp)
		if err != nil {
			fmt.Println(err)
		}
	}()

	err = fs.WalkDir(filesystem, ".", func(path string, d fs.DirEntry, err error) error {
		tmpPath := filepath.Join(tmp, path)
		if tmpPath == tmp {
			return nil
		}
		if d.IsDir() {
			return os.Mkdir(tmpPath, 0755)
		}

		src, err := filesystem.Open(path)
		if err != nil {
			return err
		}
		defer func() {
			_ = src.Close()
		}()
		dst, err := os.Create(tmpPath)
		if err != nil {
			return err
		}
		defer func() {
			_ = dst.Close()
		}()
		_, err = io.Copy(dst, src)
		return err
	})
	if err != nil {
		panic(err)
	}
	f(tmp)
}
