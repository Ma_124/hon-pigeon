# Example Certificates
Example certificates used in the unit tests.

## Root (CA)
Valid from `2000-01-01T00:00:00Z` until `2020-12-31T23:59:59Z`.

## example.com
Valid from `2010-01-01T00:00:00Z` until `2020-12-31T23:59:59Z`.

## localhost
Valid from `2010-01-01T00:00:00Z` until `2020-12-31T23:59:59Z`.
