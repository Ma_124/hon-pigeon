package pigeon

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"sync"
	"time"

	"hon-pigeon/config"
	"hon-pigeon/handler"
	"hon-pigeon/matcher"

	"github.com/caddyserver/certmagic"
)

type pServer struct {
	Server             http.Server
	DefaultCertDomain  string
	AllowedCertDomains *matcher.DomainMatcher[struct{}]
}

func (p *Pigeon) initServers(cfg *config.Config) error {
	p.httpsServers = make([]*pServer, 0, len(cfg.Servers))
	for srvName, srvCfg := range cfg.Servers {
		err := func() error {
			h, err := handler.NewHandler(cfg, srvCfg)
			if err != nil {
				return fmt.Errorf("%s: %w", srvName, err)
			}
			var defaultCertDomain string
			var allowedCertDomains *matcher.DomainMatcher[struct{}]
			if srvCfg.TLS != nil {
				defaultCertDomain = srvCfg.TLS.DefaultCertDomain
				if len(srvCfg.TLS.AllowedCertDomains) != 0 {
					allowedCertDomains = matcher.NewDomainMatcher[struct{}]()
					for i, domain := range srvCfg.TLS.AllowedCertDomains {
						err = allowedCertDomains.Add(domain, struct{}{})
						if err != nil {
							return fmt.Errorf("%s: allowed-cert-domains[%v]: %w", srvName, i, err)
						}
					}
					if defaultCertDomain != "" {
						if _, ok := allowedCertDomains.TryMatch(defaultCertDomain); !ok {
							return fmt.Errorf("%s: default-cert-domain is not in allowed-cert-domains", srvName)
						}
					}
				}
			}
			p.httpsServers = append(p.httpsServers, &pServer{
				Server: http.Server{
					Addr:              fmt.Sprintf(":%d", srvCfg.Port),
					Handler:           h,
					ReadHeaderTimeout: 10 * time.Second,
					ReadTimeout:       30 * time.Second,
					WriteTimeout:      2 * time.Minute,
					IdleTimeout:       5 * time.Minute,
					BaseContext:       func(listener net.Listener) context.Context { return p.rootContext },
				},
				DefaultCertDomain:  defaultCertDomain,
				AllowedCertDomains: allowedCertDomains,
			})
			return nil
		}()
		if err != nil {
			return fmt.Errorf("server %q: %w", srvName, err)
		}
	}
	if cfg.HTTPPort != 0 {
		p.httpServer = &http.Server{
			Addr:              fmt.Sprintf(":%d", cfg.HTTPPort),
			Handler:           http.HandlerFunc(handler.HttpRedirectHandler),
			ReadHeaderTimeout: 5 * time.Second,
			ReadTimeout:       5 * time.Second,
			WriteTimeout:      5 * time.Second,
			IdleTimeout:       5 * time.Second,
			BaseContext:       func(listener net.Listener) context.Context { return p.rootContext },
		}
	}
	if len(p.magic.Issuers) > 0 {
		if am, ok := p.magic.Issuers[0].(*certmagic.ACMEIssuer); ok {
			p.httpServer.Handler = am.HTTPChallengeHandler(p.httpServer.Handler)
		}
	}
	return nil
}

func (p *Pigeon) ListenAndServe() error {
	var wg sync.WaitGroup
	srvErrCh := make(chan error)
	for _, srv := range p.httpsServers {
		go func(srv *pServer) {
			defer wg.Done()

			addr := srv.Server.Addr
			ln, err := tls.Listen("tcp", addr, p.tlsConfig(srv))
			if err != nil {
				srvErrCh <- fmt.Errorf("%s: %w", addr, err)
				return
			}

			err = srv.Server.Serve(ln)
			if err != nil && err != http.ErrServerClosed {
				srvErrCh <- fmt.Errorf("%s: %w", addr, err)
				return
			}
		}(srv)
	}

	wg.Add(len(p.httpsServers))
	if p.httpServer != nil {
		wg.Add(1)
		go func() {
			defer wg.Done()

			err := p.httpServer.ListenAndServe()
			if err != nil && err != http.ErrServerClosed {
				srvErrCh <- fmt.Errorf("%s: %w", p.httpServer.Addr, err)
				return
			}
		}()
	}

	errCh := make(chan error)
	go func() {
		wg.Wait()
		errCh <- nil
	}()
	go func() {
		errs := &ErrorCollection{}
		first := true
		var closeErr error
		for err := <-srvErrCh; err != nil; {
			if first {
				closeErr = p.Close()
				first = false
			}
			errs.Add(err)
		}
		errs.Add(closeErr)
		errCh <- errs.IntoError()
	}()
	return <-errCh
}

func (p *Pigeon) Close() error {
	errs := &ErrorCollection{}
	errs.Add(p.httpServer.Close())
	for _, srv := range p.httpsServers {
		errs.Add(srv.Server.Close())
	}
	return errs.IntoError()
}

func (p *Pigeon) Shutdown(ctx context.Context) error {
	errs := &ErrorCollection{}
	errs.Add(p.httpServer.Shutdown(ctx))
	for _, srv := range p.httpsServers {
		errs.Add(srv.Server.Shutdown(ctx))
	}
	return errs.IntoError()
}
