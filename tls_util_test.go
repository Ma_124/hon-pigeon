package pigeon

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	_ "embed"
	"io"
	"testing"
	"time"

	"hon-pigeon/config"
	"hon-pigeon/internal/testutil"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var now time.Time
var certPool *x509.CertPool

//go:embed internal/example-certs/root.crt
var rootCrtPem []byte

//go:embed internal/example-certs/example.com.crt
var exampleComCrtPem []byte

//go:embed internal/example-certs/example.com.key
var exampleComKeyPem []byte

//go:embed internal/example-certs/localhost.crt
var localhostCrtPem []byte

//go:embed internal/example-certs/localhost.key
var localhostKeyPem []byte

func init() {
	var err error
	now, err = time.Parse(time.RFC3339, "2015-10-11T18:45:23+02:00")
	if err != nil {
		panic(err)
	}

	certPool = x509.NewCertPool()
	ok := certPool.AppendCertsFromPEM(rootCrtPem)
	if !ok {
		panic("could not add root.pem")
	}
}

type tlsTest struct {
	Config      *config.Config
	ConfPigeon  func(t *testing.T, p *Pigeon)
	Server      *pServer
	ClientTLS   *tls.Config
	ServerTLS   func(t *testing.T, cfg *tls.Config)
	DoServer    func(t *testing.T, conn *tls.Conn) error
	DoClient    func(t *testing.T, conn *tls.Conn) error
	ClientError string
	ServerError string
}

func testTLS(t *testing.T, tt *tlsTest) {
	if tt.DoServer == nil && tt.DoClient == nil {
		tt.DoServer = recvTest
		tt.DoClient = sendTest
	}

	p := &Pigeon{
		rootContext: context.Background(),
	}
	err := p.initTLS(tt.Config)
	require.NoError(t, err)
	if tt.ConfPigeon != nil {
		tt.ConfPigeon(t, p)
	}

	serverTLS := p.tlsConfig(tt.Server)
	if tt.ServerTLS != nil {
		tt.ServerTLS(t, serverTLS)
	}
	ln, err := tls.Listen("tcp", "", serverTLS)
	require.NoError(t, err)
	defer func() {
		assert.NoError(t, ln.Close())
	}()

	serverClosed := make(chan struct{})
	go func() {
		defer close(serverClosed)

		err := func() error {
			connFromClient, err := ln.Accept()
			if err != nil {
				return err
			}
			defer func() {
				assert.NoError(t, connFromClient.Close())
			}()

			return tt.DoServer(t, connFromClient.(*tls.Conn))
		}()
		testutil.AssertEqError(t, err, tt.ServerError)
	}()

	time.Sleep(1 * time.Millisecond)

	err = func() error {
		connToServer, err := tls.Dial("tcp", ln.Addr().String(), tt.ClientTLS)
		if err != nil {
			return err
		}
		defer func() {
			assert.NoError(t, connToServer.Close())
		}()

		return tt.DoClient(t, connToServer)
	}()
	testutil.AssertEqError(t, err, tt.ClientError)
	<-serverClosed
}

func addExampleComCrt(t *testing.T, p *Pigeon) {
	err := p.magic.CacheUnmanagedCertificatePEMBytes(context.Background(), exampleComCrtPem, exampleComKeyPem, nil)
	assert.NoError(t, err)
}

func addLocalhostCrt(t *testing.T, p *Pigeon) {
	err := p.magic.CacheUnmanagedCertificatePEMBytes(context.Background(), localhostCrtPem, localhostKeyPem, nil)
	assert.NoError(t, err)
}

func clientWithSNI(host string) *tls.Config {
	return &tls.Config{
		Time: func() time.Time {
			return now
		},
		RootCAs:    certPool,
		NextProtos: nil,
		ServerName: host,
	}
}

func clientWithSNINoVerify(host string) *tls.Config {
	cfg := clientWithSNI(host)
	cfg.InsecureSkipVerify = true
	return cfg
}

func sendTest(t *testing.T, conn *tls.Conn) error {
	_, err := conn.Write([]byte("test"))
	if err != nil {
		return err
	}
	return conn.CloseWrite()
}

func recvTest(t *testing.T, conn *tls.Conn) error {
	data, err := io.ReadAll(conn)
	if err != nil {
		return err
	}
	assert.Equal(t, "test", string(data))
	return nil
}
