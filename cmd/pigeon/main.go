package main

import (
	"fmt"

	pigeon "hon-pigeon"
)

func main() {
	err := pigeon.Start()
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}
}
