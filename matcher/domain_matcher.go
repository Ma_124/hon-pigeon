package matcher

import (
	"fmt"
	"regexp"
	"strings"
)

var directDomainPattern = regexp.MustCompile(`^[a-z0-9-]+(\.[a-z0-9-]+)*$`)
var wildcardDomainPattern = regexp.MustCompile(`^\*(\.[a-z0-9-]+)+$`)

type domainPatternWithValue[V any] struct {
	pattern *regexp.Regexp
	v       V
}
type DomainMatcher[V any] struct {
	direct   map[string]V
	patterns []domainPatternWithValue[V]
}

func NewDomainMatcher[V any]() *DomainMatcher[V] {
	return &DomainMatcher[V]{direct: map[string]V{}}
}

func (m *DomainMatcher[V]) Add(pattern string, v V) error {
	if directDomainPattern.MatchString(pattern) {
		_, ok := m.direct[pattern]
		if ok {
			return fmt.Errorf("already registered value for %q pattern", pattern)
		}
		m.direct[pattern] = v
	} else if wildcardDomainPattern.MatchString(pattern) {
		m.patterns = append(m.patterns, domainPatternWithValue[V]{
			pattern: regexp.MustCompile(`^[a-z0-9-]+` + regexp.QuoteMeta(pattern[1:]) + `$`),
			v:       v,
		})
	} else {
		return fmt.Errorf("patterns must be literal or have a leading wildcard")
	}
	return nil
}

func (m *DomainMatcher[V]) TryMatch(domain string) (V, bool) {
	domain = strings.TrimSuffix(strings.ToLower(domain), ".")
	v, ok := m.direct[domain]
	if ok {
		return v, ok
	}
	for _, pat := range m.patterns {
		if pat.pattern.MatchString(domain) {
			return pat.v, true
		}
	}
	{
		var v V
		return v, false
	}
}
