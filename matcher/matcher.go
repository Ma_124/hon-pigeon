package matcher

type Splitter func(inp string) func() (segment string, hasNext bool)

type Matcher[V any] struct {
	splitter Splitter
	tree     map[string]node[V]
}

func NewMatcher[V any](splitter Splitter) *Matcher[V] {
	return &Matcher[V]{
		splitter: splitter,
	}
}

func (m *Matcher[V]) Add(pattern string, v V) error {
	panic("TODO")
}

func (m *Matcher[V]) TryMatch(path string) (V, bool) {
	panic("TODO")
}

type node[V any] interface {
	node()
}

type exact[V any] struct {
	v V
}

func (e exact[V]) node() {
	//TODO implement me
	panic("implement me")
}

type wildcard[V any] struct {
	v V
}

func (e wildcard[V]) node() {
	//TODO implement me
	panic("implement me")
}
