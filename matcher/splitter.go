package matcher

import "strings"

func splitPath(path string) func() (segment string, remaining string) {
	path = strings.TrimPrefix(path, "/")
	return func() (this string, remaining string) {
		i := strings.IndexByte(path, '/')
		if i < 0 {
			this = path
			path = ""
		} else {
			this = path[:i]
			path = path[i+1:]
		}
		remaining = path
		return
	}
}

func splitDomain(remaining string) func() (segment string, hasNext bool) {
	remaining = strings.TrimSuffix(remaining, ".")
	return func() (this string, hasNext bool) {
		i := strings.LastIndexByte(remaining, '.')
		if i < 0 {
			this = remaining
			remaining = ""
			hasNext = false
		} else {
			this = remaining[i+1:]
			remaining = remaining[:i]
			hasNext = true
		}
		return
	}
}
