package matcher

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var directDomainTests = []struct {
	name      string
	domain    string
	wantMatch bool
}{
	{"empty", "", false},
	{"non-ascii", "a\x80b", false},
	{"ampersand", "a&b", false},

	{"ccTLD", "eu", true},
	{"gTLD", "com", true},
	{"special TLD", "local", true},

	{"TLD with leading dot", ".eu", false},

	{"SLD", "example.com", true},
	{"TLD", "test.example.com", true},
	{"FLD", "bar.foo.example.com", true},
	{"hyphen and numbers", "a0-9b.com", true},

	{"SLD with leading dot", ".example.com", false},
	{"SLD with ampersand", "exa&mple.com", false},
	{"SLD with leading ampersand", "&.example.com", false},

	{"double dot", "example..com", false},
}

var wildcardDomainTests = []struct {
	name      string
	domain    string
	wantMatch bool
}{
	{"empty", "", false},
	{"non-ascii", "a\x80b", false},
	{"ampersand", "a&b", false},

	{"everything", "*", false},

	{"ccTLD", "*.eu", true},
	{"gTLD", "*.com", true},
	{"special TLD", "*.local", true},

	{"SLD", "*.example.com", true},
	{"TLD", "*.test.example.com", true},
	{"FLD", "*.bar.foo.example.com", true},
	{"hyphen and numbers", "*.a0-9b.com", true},

	{"SLD with leading dot", ".*.example.com", false},
	{"SLD with ampersand", "*.exa&mple.com", false},
	{"SLD with leading ampersand", "&.example.com", false},

	{"double dot", "*.example..com", false},
}

func TestDomainMatcher_directDomainPattern(t *testing.T) {
	for _, tt := range directDomainTests {
		t.Run(tt.name, func(t *testing.T) {
			got := directDomainPattern.MatchString(tt.domain)
			assert.Equal(t, tt.wantMatch, got)
		})
	}
	t.Run("mutex with wildcardDomainPattern", func(t *testing.T) {
		for _, tt := range wildcardDomainTests {
			if !tt.wantMatch {
				continue
			}
			t.Run(tt.name, func(t *testing.T) {
				got := directDomainPattern.MatchString(tt.domain)
				if got {
					t.Errorf("directDomainPattern matched even though wildcardDomainPattern should match as well")
				}
			})
		}
	})
}

func TestDomainMatcher_wildcardDomainPattern(t *testing.T) {
	for _, tt := range wildcardDomainTests {
		t.Run(tt.name, func(t *testing.T) {
			got := wildcardDomainPattern.MatchString(tt.domain)
			assert.Equal(t, tt.wantMatch, got)
		})
	}
	t.Run("mutex with directDomainTests", func(t *testing.T) {
		for _, tt := range directDomainTests {
			if !tt.wantMatch {
				continue
			}
			t.Run(tt.name, func(t *testing.T) {
				got := wildcardDomainPattern.MatchString(tt.domain)
				if got {
					t.Errorf("wildcardDomainPattern matched even though directDomainPattern should match as well")
				}
			})
		}
	})
}

func TestDomainMatcher_TryMatch(t *testing.T) {
	tests := []struct {
		name    string
		pattern string
		domain  string
		want    bool
	}{
		{"literal TLD", "local", "local", true},
		{"literal TLD with root dot", "local", "local.", true},
		{"literal TLD with root double dot", "local", "local..", false},
		{"SLD with literal TLD", "com", "example.com", false},
		{"SLD with SLD wildcard", "*.com", "example.com", true},
		{"SLD with SLD wildcard and root dot", "*.com", "example.com.", true},
		{"SLD with SLD wildcard and root double dot", "*.com", "example.com..", false},
		{"TLD with SLD wildcard", "*.com", "test.example.com", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := NewDomainMatcher[int]()
			err := m.Add(tt.pattern, 42)
			require.NoError(t, err)

			v, got := m.TryMatch(tt.domain)
			assert.Equal(t, tt.want, got)
			if got {
				assert.Equal(t, 42, v)
			} else {
				assert.Equal(t, 0, v)
			}
		})
	}
}

func TestDomainMatcher_Add(t *testing.T) {
	m := NewDomainMatcher[bool]()
	err := m.Add("example.com", true)
	assert.NoError(t, err)
	err = m.Add("example.com", true)
	assert.Error(t, err)
	err = m.Add("*.int", true)
	assert.NoError(t, err)
	err = m.Add("test..local", true)
	assert.Error(t, err)

	if v, ok := m.TryMatch("test.int"); !v || !ok {
		t.Errorf("test.int did not match")
	}
	if v, ok := m.TryMatch("example.com"); !v || !ok {
		t.Errorf("example.com did not match")
	}
	if v, ok := m.TryMatch("test.com"); v || ok {
		t.Errorf("test.com did match")
	}
}
