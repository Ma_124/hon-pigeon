package matcher

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_splitPath(t *testing.T) {
	tests := []struct {
		name string
		path string
	}{
		{
			name: "empty",
			path: "",
		},
		{
			name: "single slash",
			path: "/",
		},
		{
			name: "double slash",
			path: "//",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			next := splitPath(tt.path)
			segments := []string{}
			for {
				seg, remaining := next()
				require.Equal(t, tt.path, seg+"/"+remaining)
				segments = append(segments, seg)
				if remaining == "" {
					break
				}
			}
			require.Equal(t, strings.Split(tt.path, "/"), segments)
		})
	}
}
