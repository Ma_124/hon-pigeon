package pigeon

import (
	"context"
	"fmt"
	"math/rand"
	"strconv"
	"testing"

	"hon-pigeon/config"
	"hon-pigeon/matcher"

	"github.com/caddyserver/certmagic"
	"github.com/stretchr/testify/assert"
)

func TestPigeonTLS(t *testing.T) {
	t.Run("simple, server to client", func(t *testing.T) {
		testTLS(t, &tlsTest{
			Config:     &config.Config{},
			ConfPigeon: addExampleComCrt,
			Server:     &pServer{},
			ClientTLS:  clientWithSNI("example.com"),
			DoServer:   sendTest,
			DoClient:   recvTest,
		})
	})
	t.Run("simple, client to server", func(t *testing.T) {
		testTLS(t, &tlsTest{
			Config:     &config.Config{},
			ConfPigeon: addExampleComCrt,
			Server:     &pServer{},
			ClientTLS:  clientWithSNI("example.com"),
			DoServer:   recvTest,
			DoClient:   sendTest,
		})
	})
	t.Run("no certificates", func(t *testing.T) {
		testTLS(t, &tlsTest{
			Config:      &config.Config{},
			ConfPigeon:  nil,
			Server:      &pServer{},
			ClientTLS:   clientWithSNI("example.com"),
			ServerError: "no certificate available for 'example.com'",
			ClientError: "remote error: tls: internal error",
		})
	})
	t.Run("no matching certificates", func(t *testing.T) {
		testTLS(t, &tlsTest{
			Config:      &config.Config{},
			ConfPigeon:  addLocalhostCrt,
			Server:      &pServer{},
			ClientTLS:   clientWithSNI("example.com"),
			ServerError: "no certificate available for 'example.com'",
			ClientError: "remote error: tls: internal error",
		})
	})
	t.Run("no matching certificates but fallback", func(t *testing.T) {
		testTLS(t, &tlsTest{
			Config:      &config.Config{},
			ConfPigeon:  addLocalhostCrt,
			Server:      &pServer{DefaultCertDomain: "localhost"},
			ClientTLS:   clientWithSNI("example.com"),
			ServerError: "remote error: tls: bad certificate",
			ClientError: "x509: certificate is valid for localhost, not example.com",
		})
	})
	t.Run("no explicitly matching certificates", func(t *testing.T) {
		testTLS(t, &tlsTest{
			Config:      &config.Config{},
			ConfPigeon:  addExampleComCrt,
			Server:      &pServer{AllowedCertDomains: matcher.NewDomainMatcher[struct{}]()},
			ClientTLS:   clientWithSNI("example.com"),
			DoServer:    recvTest,
			DoClient:    sendTest,
			ServerError: `"": domain "example.com" not allowed`,
			ClientError: "remote error: tls: internal error",
		})
	})
	t.Run("no explicitly matching certificates but fallback", func(t *testing.T) {
		testTLS(t, &tlsTest{
			Config:     &config.Config{},
			ConfPigeon: addLocalhostCrt,
			Server:     &pServer{AllowedCertDomains: matcher.NewDomainMatcher[struct{}](), DefaultCertDomain: "localhost"},
			ClientTLS:  clientWithSNINoVerify("example.com"),
		})
	})
	t.Run("no matching certificates but fallback and lenient client", func(t *testing.T) {
		testTLS(t, &tlsTest{
			Config:     &config.Config{},
			ConfPigeon: addLocalhostCrt,
			Server:     &pServer{DefaultCertDomain: "localhost"},
			ClientTLS:  clientWithSNINoVerify("example.com"),
		})
	})
}

func TestPigeon_initTLS(t *testing.T) {
	t.Run("default ACME unchanged", func(t *testing.T) {
		defaultACME := certmagic.DefaultACME
		p := Pigeon{rootContext: context.Background()}
		err := p.initTLS(&config.Config{
			TLS: &config.TLS{
				ACME: &config.ACME{
					Mail: strconv.Itoa(rand.Int()),
				},
			},
		})
		assert.NoError(t, err)
		assert.Equal(t, certmagic.DefaultACME, defaultACME)
		assert.Equal(t, fmt.Sprintf("%T", certmagic.DefaultACME), fmt.Sprintf("%T", defaultACME))
		assert.NotEqual(t, p.magic.Issuers[0], &defaultACME)
		assert.Equal(t, fmt.Sprintf("%T", p.magic.Issuers[0]), fmt.Sprintf("%T", &defaultACME))
	})
	t.Run("default magic unchanged", func(t *testing.T) {
		defaultCfg := certmagic.Default
		p := Pigeon{rootContext: context.Background()}
		err := p.initTLS(&config.Config{
			TLS: &config.TLS{
				ACME: &config.ACME{
					Mail: strconv.Itoa(rand.Int()),
				},
			},
		})
		assert.NoError(t, err)
		assert.Equal(t, certmagic.Default, defaultCfg)
		assert.Equal(t, fmt.Sprintf("%T", certmagic.Default), fmt.Sprintf("%T", defaultCfg))
		assert.NotEqual(t, p.magic, &defaultCfg)
		assert.Equal(t, fmt.Sprintf("%T", p.magic), fmt.Sprintf("%T", &defaultCfg))
	})
	t.Run("lets encrypt", func(t *testing.T) {
		assert.Equal(t, certmagic.DefaultACME.CA, certmagic.LetsEncryptProductionCA)
		assert.Equal(t, certmagic.DefaultACME.TestCA, certmagic.LetsEncryptStagingCA)
	})
	t.Run("prod directory", func(t *testing.T) {
		p := Pigeon{rootContext: context.Background()}
		err := p.initTLS(&config.Config{
			TLS: &config.TLS{
				ACME: &config.ACME{
					Staging: false,
				},
			},
		})
		assert.NoError(t, err)
		assert.Equal(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).CA, certmagic.LetsEncryptProductionCA)
	})
	t.Run("staging", func(t *testing.T) {
		p := Pigeon{rootContext: context.Background()}
		err := p.initTLS(&config.Config{
			TLS: &config.TLS{
				ACME: &config.ACME{
					Staging: true,
				},
			},
		})
		assert.NoError(t, err)
		assert.Equal(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).CA, certmagic.LetsEncryptStagingCA)
	})
	t.Run("not agreed", func(t *testing.T) {
		p := Pigeon{rootContext: context.Background()}
		err := p.initTLS(&config.Config{
			TLS: &config.TLS{
				ACME: &config.ACME{
					Agreed: false,
				},
			},
		})
		assert.NoError(t, err)
		assert.False(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).Agreed)
	})
	t.Run("no http port", func(t *testing.T) {
		p := Pigeon{rootContext: context.Background()}
		err := p.initTLS(&config.Config{
			TLS: &config.TLS{
				ACME: &config.ACME{},
			},
		})
		assert.NoError(t, err)
		assert.True(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).DisableHTTPChallenge)
		assert.False(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).DisableTLSALPNChallenge)
	})
	t.Run("http port", func(t *testing.T) {
		p := Pigeon{rootContext: context.Background()}
		err := p.initTLS(&config.Config{
			HTTPPort: 30080,
			TLS: &config.TLS{
				ACME: &config.ACME{},
			},
		})
		assert.NoError(t, err)
		assert.False(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).DisableHTTPChallenge)
		assert.False(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).DisableTLSALPNChallenge)
		assert.Equal(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).AltHTTPPort, 30080)
	})
	t.Run("disable http challenge", func(t *testing.T) {
		p := Pigeon{rootContext: context.Background()}
		err := p.initTLS(&config.Config{
			TLS: &config.TLS{
				ACME: &config.ACME{
					DisableHTTPChallenge: true,
				},
			},
		})
		assert.NoError(t, err)
		assert.True(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).DisableHTTPChallenge)
		assert.False(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).DisableTLSALPNChallenge)
	})
	t.Run("disable alpn challenge", func(t *testing.T) {
		p := Pigeon{rootContext: context.Background()}
		err := p.initTLS(&config.Config{
			HTTPPort: 80,
			TLS: &config.TLS{
				ACME: &config.ACME{
					DisableALPNChallenge: true,
				},
			},
		})
		assert.NoError(t, err)
		assert.False(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).DisableHTTPChallenge)
		assert.True(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).DisableTLSALPNChallenge)
	})
	t.Run("different alpn port", func(t *testing.T) {
		p := Pigeon{rootContext: context.Background()}
		err := p.initTLS(&config.Config{
			HTTPPort: 80,
			TLS: &config.TLS{
				ACME: &config.ACME{
					ALPNPort: 30443,
				},
			},
		})
		assert.NoError(t, err)
		assert.False(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).DisableHTTPChallenge)
		assert.False(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).DisableTLSALPNChallenge)
		assert.Equal(t, p.magic.Issuers[0].(*certmagic.ACMEIssuer).AltTLSALPNPort, 30443)
	})
}
