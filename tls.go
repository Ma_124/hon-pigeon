package pigeon

import (
	"crypto/tls"
	"fmt"
	"strings"

	"hon-pigeon/config"

	"github.com/caddyserver/certmagic"
)

func (p *Pigeon) initTLS(cfg *config.Config) error {
	if cfg.TLS == nil {
		cfg.TLS = &config.TLS{}
	}
	if cfg.TLS.Certificates == nil {
		cfg.TLS.Certificates = &config.Certificates{}
	}

	// ensure copy by this being not a pointer
	var iss certmagic.ACMEIssuer
	if cfg.TLS.ACME != nil {
		iss = certmagic.DefaultACME
		iss.Agreed = cfg.TLS.ACME.Agreed
		iss.Email = cfg.TLS.ACME.Mail
		if cfg.TLS.ACME.Staging {
			iss.CA = iss.TestCA
		}
		if cfg.HTTPPort == 0 || cfg.TLS.ACME.DisableHTTPChallenge {
			iss.DisableHTTPChallenge = true
		} else {
			iss.AltHTTPPort = int(cfg.HTTPPort)
		}
		if cfg.TLS.ACME.DisableALPNChallenge {
			iss.DisableTLSALPNChallenge = true
		}
		if cfg.TLS.ACME.ALPNPort != 0 {
			iss.AltTLSALPNPort = int(cfg.TLS.ACME.ALPNPort)
		}
	}

	magicCopy := certmagic.Default
	if cfg.TLS.ACME != nil {
		magicCopy.Issuers = []certmagic.Issuer{&iss}
	}
	p.magic = &magicCopy
	p.magic = certmagic.New(
		certmagic.NewCache(certmagic.CacheOptions{
			GetConfigForCert: func(certificate certmagic.Certificate) (*certmagic.Config, error) {
				return p.magic, nil
			}}), *p.magic)
	if len(cfg.TLS.Certificates.ACME) > 0 {
		err := p.magic.ManageSync(p.rootContext, cfg.TLS.Certificates.ACME)
		if err != nil {
			return fmt.Errorf("acme: %w", err)
		}
	}
	for i, crt := range cfg.TLS.Certificates.Manual {
		err := p.magic.CacheUnmanagedCertificatePEMFile(p.rootContext, crt.Crt, crt.Key, []string{})
		if err != nil {
			return fmt.Errorf("%v (%s): %w", i, crt.Crt, err)
		}
	}
	return nil
}

func (p *Pigeon) tlsConfig(srv *pServer) *tls.Config {
	cfg := p.magic.TLSConfig()
	cfg.NextProtos = append([]string{"h2", "http/1.1"}, cfg.NextProtos...)
	cfg.GetCertificate = func(info *tls.ClientHelloInfo) (*tls.Certificate, error) {
		if srv.AllowedCertDomains != nil {
			if _, ok := srv.AllowedCertDomains.TryMatch(info.ServerName); !ok {
				if srv.DefaultCertDomain == "" {
					return nil, fmt.Errorf("%q: domain %q not allowed", srv.Server.Addr, info.ServerName)
				}
				info.ServerName = srv.DefaultCertDomain
				return p.magic.GetCertificate(info)
			}
		}

		crt, err := p.magic.GetCertificate(info)
		// fallback to default certificate
		if err != nil &&
			srv.DefaultCertDomain != "" &&
			strings.HasPrefix(err.Error(), "no certificate available for '") {
			info.ServerName = srv.DefaultCertDomain
			return p.magic.GetCertificate(info)
		}
		return crt, err
	}
	return cfg
}
