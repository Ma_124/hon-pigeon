package config

import (
	"gopkg.in/yaml.v3"
)

type Config struct {
	TLS      *TLS   `yaml:"tls"`
	HTTPPort uint16 `yaml:"http-port"`
	Servers  map[string]*Server
	Routes   map[string][]*Route
	Error    *RouteHandler
}

type TLS struct {
	ACME         *ACME `yaml:"acme"`
	Certificates *Certificates
}

type Certificates struct {
	Manual []*ManualCertificate
	ACME   []string `yaml:"acme"`
}

type ManualCertificate struct {
	Key string
	Crt string
}

type ACME struct {
	Mail                 string
	Agreed               bool
	Staging              bool
	DisableHTTPChallenge bool   `yaml:"disable-http-challenge"`
	DisableALPNChallenge bool   `yaml:"disable-alpn-challenge"`
	ALPNPort             uint16 `yaml:"alpn-port"`
}

type Server struct {
	Port   uint16
	TLS    *ServerTLS
	Routes []string
}

type ServerTLS struct {
	DefaultCertDomain  string  `yaml:"default-cert-domain"`
	AllowedCertDomains Strings `yaml:"allowed-cert-domains"`
}

type Route struct {
	Matcher  RouteMatcher  `yaml:",inline"`
	Modifier RouteModifier `yaml:",inline"`
	Handler  RouteHandler  `yaml:",inline"`
}

type RouteMatcher struct {
	Host Strings
	Path Strings
}

type RouteModifier struct {
	AddPathPrefix string `yaml:"add-path-prefix"`
}

type RouteHandler struct {
	File          string
	TemplatedFile string `yaml:"templated-file"`
	Directory     string
	Redirect      *Redirect
	ReverseProxy  string `yaml:"reverse-proxy"`
}

type Redirect struct {
	Port      uint16
	Path      string
	Permanent bool
}

type Strings []string

func (s *Strings) UnmarshalYAML(value *yaml.Node) error {
	err := value.Decode((*[]string)(s))
	if err == nil {
		return nil
	}

	var str string
	err = value.Decode(&str)
	if err != nil {
		return err
	}
	*s = []string{str}
	return nil
}
