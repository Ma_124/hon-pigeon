package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

func TestStrings_UnmarshalYAML(t *testing.T) {
	tests := []struct {
		name string
		yaml string
		want []string
	}{
		{"empty string", "key:", []string{}},
		{"string", "key: hello", []string{"hello"}},
		{"empty list", "key: []", []string{}},
		{"one string in list", `key: ["hello"]`, []string{"hello"}},
		{"two strings", `key: ["hello", "world"]`, []string{"hello", "world"}},
		{"three strings", `key: ["hello", "world", "foo"]`, []string{"hello", "world", "foo"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var data struct {
				Key Strings `yaml:"key"`
			}
			err := yaml.Unmarshal([]byte(tt.yaml), &data)
			require.NoError(t, err)
			got := []string(data.Key)
			if got == nil {
				got = []string{}
			}
			assert.Equal(t, tt.want, got)
		})
	}
	t.Run("malformed", func(t *testing.T) {
		var data struct {
			Key Strings `yaml:"key"`
		}
		err := yaml.Unmarshal([]byte(`key: {}`), &data)
		assert.Error(t, err)
	})
}
