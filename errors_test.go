package pigeon

import (
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestErrorCollection_Add(t *testing.T) {
	tests := []struct {
		name string
		e    *ErrorCollection
		msg  string
	}{
		{"empty", &ErrorCollection{}, ""},
		{"empty with nil", (&ErrorCollection{}).Add(nil), ""},
		{"single error", (&ErrorCollection{}).Add(io.EOF), "EOF"},
		{"single error with nil", (&ErrorCollection{}).Add(io.EOF).Add(nil), "EOF"},
		{"multiple errors", (&ErrorCollection{}).Add(io.EOF).Add(io.ErrUnexpectedEOF), "multiple errors:\nEOF\nunexpected EOF\n"},
		{"multiple errors with nil", (&ErrorCollection{}).Add(io.EOF).Add(nil).Add(io.ErrUnexpectedEOF), "multiple errors:\nEOF\nunexpected EOF\n"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.e.IntoError()
			if tt.msg == "" {
				assert.NoError(t, err)
			} else {
				assert.EqualError(t, err, tt.msg)
			}
		})
	}
}
